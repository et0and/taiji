# Taiji

A very simple text editor written in C, with no external dependencies

Usage: taiji `<filename>`

Controls:

    CTRL-S: Save
    CTRL-Q: Quit
    CTRL-F: Find string in file (ESC to exit search, arrows to navigate)
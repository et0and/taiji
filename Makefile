all: taiji

taiji: taiji.c
	$(CC) -o taiji taiji.c -Wall -W -pedantic -std=c99

clean:
	rm taiji
